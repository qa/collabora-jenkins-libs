def call(String defaultRepo, String defaultBranch = 'master', String subdir = null) {
    echo "checkoutCollaboraGitlab(${defaultRepo}, ${defaultBranch}, ${subdir})..."

    if (env.gitlabSourceRepoSshUrl == null) {
        env.gitlabSourceRepoSshUrl = "git@gitlab.collabora.com:${defaultRepo}"
        env.gitlabSourceBranch = defaultBranch
        env.gitlabTargetBranch = defaultBranch
        echo "Using default source repo ${defaultRepo} branch ${defaultBranch}"
    }
    else {
        echo "Using non-default source repo ${env.gitlabSourceRepoSshUrl} branch ${env.gitlabSourceBranch} instead of ${defaultRepo} branch ${defaultBranch}"
    }

    if (env.gitlabTargetRepoSshUrl == null) {
        echo "Duplicating source repo to target repo"
        env.gitlabTargetRepoSshUrl = env.gitlabSourceRepoSshUrl
    }
    else {
        echo "Using target repo ${env.gitlabTargetRepoSshUrl} branch ${env.gitlabTargetBranch}"
    }

    def extensions = []

    extensions << [$class: 'PruneStaleBranch']

    if (env.gitlabSourceBranch != env.gitlabTargetBranch ||
        env.gitlabTargetRepoSshUrl != env.gitlabSourceRepoSshUrl) {
        extensions << [$class: 'PreBuildMerge',
                       options: [fastForwardMode: 'FF',
                                 mergeRemote: 'target',
                                 mergeStrategy: 'DEFAULT',
                                 mergeTarget: "${env.gitlabTargetBranch}"]]
    }

    if (subdir != null) {
        extensions << [$class: 'RelativeTargetDirectory',
                       relativeTargetDir: subdir]
    }

    checkout changelog: true, poll: true, scm: [
          $class: 'GitSCM',
          branches: [[name: "origin/${env.gitlabSourceBranch}"]],
          extensions: extensions,
          userRemoteConfigs: [[name: 'target', url: "${env.gitlabTargetRepoSshUrl}", credentialsId: 'GITLABSSHKEY'],
                              [name: 'origin', url: "${env.gitlabSourceRepoSshUrl}", credentialsId: 'GITLABSSHKEY']]
    ]
    echo "checkoutCollaboraGitlab done"
}
